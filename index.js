// CRUD Operations
/*
    - CRUD operations are the heart of any backend application.
    - Mastering the CRUD operations is essential for any developer.
    - This helps in building character and increasing exposure to logical statements that will help us manipulate our data.
    - Mastering the CRUD operations of any language makes us a valuable developer and makes the work easier for us to deal with huge amounts of information.
*/

// [SECRTION] Inserting Documents (CREATE)

/*
    Syntax:
        -db.collectionName.insertOne({object});
*/

db.users.insertOne({
    firstName: "Jane",
    lastName: "Doe",
    age: 21,
    contact: {
        phone: "8765 4321",
        email: "janedoe@mail.com"
    },
    courses: ["CSS", "Javascript", "Python"],
    department: "none"
});

// Insert Many


db.users.insertMany([
    {
        firstName: "Stephen",
        lastName: "Hawking",
        age: 76,
        contact: {
            phone: "87000",
            email: "stephenhawking@mail.com"
        },
        couses: ["Python", "React", "PHP"],
        department: "none"
    },
    {
        firstName: "Neil",
        lastName: "Armstrong",
        age: 82,
        contact: {
            phone: "87894561",
            email: "neilarmstrong@mail.com"
        },
        couses: ["React", "Laravel", "MongoDB"],
        department: "none"
    }
]);

db.users.insert({
    firstName: "Test",
    lastName: "Test",
    age: 0,
    contact: {
        phone: "00000000",
        email: "test@gmail.com"
    },
    courses: [],
    department: "none"
});


// [SECTION] Finding Documents (READ)

/*
    -db.collectionName.find();
    -db.collectionName.find({ field: value });
*/

db.users.find();
db.users.find({firstName: "Stephen"});

//  --------------------

// db.users.deleteOne({
//     firstName: "Jane"
// });


// [SECTION] Updating documents (UPDATE)

db.users.updateOne(
    { firstName: "Test"},
    {
        $set : {
            firstName: "Bill",
            lastName: "Gates",
            age: 65,
            contact: {
                phone: "12345678",
                email: "bill@mail.com"
            },
            courses: ["PHP", "Laravel", "HTML"],
            department: "Operations",
            status: "active"

        }
    }
)

db.users.find({firstName: "Bill"});

// Updating multiple documents
/*
    -Syntax:
    -db.collectionName.updateMany({criteria}, ($set: {field: value}));
*/

db.users.updateMany(
    {department: "none"},
    {
        $set: {department: "HR"}
    }
    );

db.users.find();

// Replace One
/*
    -Can be used if replacing the whole document is necessary
*/

db.users.replaceOne(
    {firstName: "Bill"},
    {
        firstName: "Billy",
        lastName: "Crawford",
        age: 30,
        contact: {
            phone: "12345678",
            email: "billy@mail.com"
        },
        courses: ["PHP", "Laravel", "HTML"],
        department: "Operations",
        status: "active"
    }
);

db.users.deleteOne({
    firstName: "Jane"
});

db.users.deleteMany({
    firstName: "Jane"
});